const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new GW schema
const heartbeatSchema = new Schema({
    gw_uuid: {
        type: String,
        required: [true, "Id is Required"]
    },

    timestamp: {
        type: String
    },

    status: {
        type: String
    }
    //,
   // option: { useNewUrlParser: true }

});

const Heartbeat = mongoose.model('heartbeat', heartbeatSchema);

module.exports = Heartbeat;