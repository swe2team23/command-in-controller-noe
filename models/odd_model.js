const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new On-Demand CPU schema
const OddSchema = new Schema({
    gw_uuid: {
        type: String,
        required: [true, "Id is Required"]
    },

    queue: {
        type: [String]
    }
});

const Odd = mongoose.model('odd', OddSchema);

module.exports = Odd;