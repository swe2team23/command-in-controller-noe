const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new GW schema
const gatewaySchema = new Schema({
    gw_uuid: {
        type: String,
        required: [true, "Id is Required"]
    },

    status: {
        type: String
    }

});

const Gateway = mongoose.model('gateway', gatewaySchema);

module.exports = Gateway;