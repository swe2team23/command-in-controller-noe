const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new CpuTest Schema
const cpuTestSchema = new Schema({
    gw_uuid: {
        type: String,
        required: [true, "Id is Required"]
    },

    timestamp : {
        type: String
    },

    int_inititialization: {
        type: String
    },

    init_addition: {
        type: String
    },

    init_multiplication: {
        type: String
    },

    init_subtraction: {
        type: String
    },

    init_division: {
        type: String
    },

    init_mod: {
        type: String
    },

    float_initialization: {
        type: String
    },

    float_addition: {
        type: String
    },

    float_mult: {
        type: String
    },

    float_subtraction: {
        type: String
    },

    float_division: {
        type: String
    },

    float_mod: {
        type: String
    }

    //,
    // option: { useNewUrlParser: true }

});

const CpuTest = mongoose.model('cpuTest', cpuTestSchema);

module.exports = CpuTest;