const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new GW schema
const usersSchema = new Schema({
    user_id: {
        type: String,
        required: [true, "Id is Required"]
    },

    name: {
        type: String
    },

    email: {
        type: String
    }
    //,
    // option: { useNewUrlParser: true }

});

const Heartbeat = mongoose.model('heartbeat', heartbeatSchema);

module.exports = Heartbeat;