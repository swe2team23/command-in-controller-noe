const express = require('express');
const odd_router = express.Router({});
const Odd = require('../models/odd_model');


var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
odd_router.get('/odd', function (req, res) {
    Odd.find({}).then(function(odd){
        res.send(odd);
    });
    /*
        mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
            if (err) throw err;
            conn.db('team23').collection('heartbeats').find({}).toArray(function(err, result) {
                if (err) throw err;
                res.json(result);
            });
        });*/
});

odd_router.get('/odd/:gw_uuid', function (req, res) {
    Odd.find({gw_uuid : req.params.gw_uuid}).then(function(odd){
        res.send(odd);
    });
    /*
        mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
            if (err) throw err;
            conn.db('team23').collection('heartbeats').find({}).toArray(function(err, result) {
                if (err) throw err;
                res.json(result);
            });
        });*/
});

odd_router.post('/odd', function (req, res) {
  //  var odd = new Odd(req.body);
  //  odd.save();
    var odd = new Odd(req.body);
    odd.save();

    res.send(odd)
});


/*if (!req.body.gw_uuid) {
    res.status(422).json({'error': 'gw_uuid not set!'});
    return false;
}
const gw_uuid = req.body.gw_uuid;

if (!req.body.timestamp) {
    res.status(423).json({'error' : 'timestamp not set!'});
    return false;
}
const timestamp = req.body.timestamp;


if (!req.body.status) {
    res.status(424).json({'error' : 'status not set!'});
    return false;
}
const status = req.body.status;
*/



/*mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
     if (err) throw err;
     conn.db('team23').collection('heartbeats').insert(req.body);
 });*/

//res.status(201).json(req.body);


//});

odd_router.put('/odd', function (req, res) {
    Odd.update(req.body, {queue : req.body.queue});
    res.send(req.body);
});

odd_router.delete('/odd', function (req, res) {
    Odd.delete(req.body).then(function(odd) {
        res.send(req.body);
    });

    //res.status(202).send();
    //process.exit();
});

odd_router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

odd_router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = odd_router;