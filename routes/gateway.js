const express = require('express');
const gw_router = express.Router({});
const Gateway = require('../models/gateway_model');

var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
gw_router.get('/gateway', function (req, res) {
    Gateway.find({}).then(function(gateway){
        res.send(gateway);
    });

});

gw_router.post('/gateway', function (req, res) {
    var gateway = new Gateway(req.body);
    gateway.save();


    //Heartbeat.create(req.body).then(function(heartbeat){
    //res.send(heartbeat)
    res.send(gateway);
});


/*if (!req.body.gw_uuid) {
    res.status(422).json({'error': 'gw_uuid not set!'});
    return false;
}
const gw_uuid = req.body.gw_uuid;

if (!req.body.timestamp) {
    res.status(423).json({'error' : 'timestamp not set!'});
    return false;
}
const timestamp = req.body.timestamp;


if (!req.body.status) {
    res.status(424).json({'error' : 'status not set!'});
    return false;
}
const status = req.body.status;
*/



/*mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
     if (err) throw err;
     conn.db('team23').collection('heartbeats').insert(req.body);
 });*/

//res.status(201).json(req.body);


//});

gw_router.put('/', function (req, res) {
    res.status(202).send();
});

gw_router.delete('/', function (req, res) {
    res.status(202).send();
    process.exit();
});

gw_router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

gw_router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = gw_router;
