const express = require('express');
const router = express.Router({});
const Heartbeat = require('../models/heartbeat_model');
const Odd = require('../models/odd_model');


var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
router.get('/heartbeat', function (req, res) {
    Heartbeat.find({}).then(function(heartbeat){
        res.send(heartbeat);
    });
});

router.post('/heartbeat', function (req, res, next) {
    // var heartbeat = new Heartbeat(req.body);
    //  heartbeat.save();
    Heartbeat.create(req.body).then(function(heartbeat) {
        res.send(heartbeat);

    }).catch(next);
    /*   var query = {gw_uuid : req.body.gw_uuid};
       Odd.find(query).then(function(odd) {
           Odd.update(query, {queue : []});
           res.send(odd.queue);
       });*/

});


/*if (!req.body.gw_uuid) {
    res.status(422).json({'error': 'gw_uuid not set!'});
    return false;
}
const gw_uuid = req.body.gw_uuid;

if (!req.body.timestamp) {
    res.status(423).json({'error' : 'timestamp not set!'});
    return false;
}
const timestamp = req.body.timestamp;


if (!req.body.status) {
    res.status(424).json({'error' : 'status not set!'});
    return false;
}
const status = req.body.status;
*/



/*mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
     if (err) throw err;
     conn.db('team23').collection('heartbeats').insert(req.body);
 });*/

//res.status(201).json(req.body);


//});

router.put('/', function (req, res) {
    res.status(202).send();
});

router.delete('/heartbeat/:id', function (req, res, next) {
    console.log(req.params.id);
    req.send({type: DELETE});
    res.status(202).send();
    process.exit();
});

router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
