const express = require('express');
const memRouter = express.Router({});
const MemoryTest = require('../models/memoryTest_model');


var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
memRouter.get('/', function (req, res) {
    MemoryTest.find({}).then(function(memoryTest){
        res.send(memoryTest);
    });

});


memRouter.get('/:gw_uuid', function (req, res) {
    MemoryTest.find({gw_uuid : req.params.gw_uuid}).then(function(memoryTest){
        res.send(memoryTest);
    });
});

memRouter.post('/', function (req, res) {
    var memoryTest = new MemoryTest(req.body);
    memoryTest.save();


    //Heartbeat.create(req.body).then(function(heartbeat){
    //res.send(heartbeat)
    res.send(memoryTest);
});


/*if (!req.body.gw_uuid) {
    res.status(422).json({'error': 'gw_uuid not set!'});
    return false;
}
const gw_uuid = req.body.gw_uuid;

if (!req.body.timestamp) {
    res.status(423).json({'error' : 'timestamp not set!'});
    return false;
}
const timestamp = req.body.timestamp;


if (!req.body.status) {
    res.status(424).json({'error' : 'status not set!'});
    return false;
}
const status = req.body.status;
*/



/*mongodb.connect('mongodb://localhost/testDB', function(err, conn) {
     if (err) throw err;
     conn.db('team23').collection('heartbeats').insert(req.body);
 });*/

//res.status(201).json(req.body);


//});

memRouter.put('/', function (req, res) {
    res.status(202).send();
});

memRouter.delete('/', function (req, res) {
    res.status(202).send();
    process.exit();
});

memRouter.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

memRouter.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = memRouter;
